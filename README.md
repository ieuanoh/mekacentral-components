# mekacentral-components
React Component Library using the MekaCentral API

Code example of a React component library I was developing whist at Reach. The goal was to produce a set of production ready components that were hooked up to the MekaCentral API and could be imported as NPM modules. I also wanted to provide easy theming for specific use cases. 

As such, the library contains a set of components hooked up to certain endpoints with built in error handling as well as a Provider component that allowed easy initialisation of an app with pre set themes. 

I used this project as an opportunity to play around with the React Hooks API along with React's built in Context API. With Hooks, React Context really starts to become a viable state management alternative to Redux. 

Whilst you may not really be able to visualise what I was trying to achieve, I hope you can take a look at some of the code and understand what I was aiming for in this project. 

Please note that this is not a complete project, I have just provided it as an indication of what I was working on whilst at Reach Robotics.
