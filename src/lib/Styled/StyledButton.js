import styled from "styled-components";

const StyledButton = styled.input`
  padding: 1.2rem;
  border: none;
  margin: 1rem 0;
  font-size: 1rem;
  letter-spacing: 0.1rem;
  color: white;
  font-weight: bold;
  background-image: ${props =>
    props.gradient
      ? `linear-gradient(90deg, ${props.theme.primary}, ${
          props.theme.tertiary
        })`
      : "none"};
  background-color: ${props => props.theme.secondary};
  transition: 0.4s all;
  border-radius: 5px;
  text-transform: uppercase;
  letter-spacing: 0.05em;
  font-weight: 700;

  :hover {
    transform: translateY(-3px);
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.08);
    cursor: pointer;
  }
`;

export default StyledButton;
