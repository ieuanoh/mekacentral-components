import React, { useState } from "react";
import { Input, Container, ShowButton } from "./styled";

const StyledInput = ({ ...props }) => {
  // Overrides the input type so that a password can be shown
  const [inputType, updateInput] = useState("");

  const toggleInput = () => {
    inputType ? updateInput("") : updateInput("text");
  };

  return (
    <Container>
      <Input
        type={inputType || props.type}
        onChange={props.onChange}
        placeholder={props.placeholder}
        value={props.value}
        name={props.name}
        ref={props.forwardedRef}
      />
      {props.type === "password" && (
        <ShowIcon active={inputType} onClick={() => toggleInput()} />
      )}
    </Container>
  );
};

// Svg component
const ShowIcon = props => (
  <ShowButton active={props.active}>
    <svg viewBox="0 0 24 24" {...props}>
      <path d="M4 4c-1.1 0-2 .9-2 2v8c0 1.1.9 2 2 2h6.1c1.3-1.5 3.5-3 5.9-3 2.1 0 4 1.2 5.3 2.5.4-.4.7-.9.7-1.5V6c0-1.1-.9-2-2-2H4zm2 4c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm6 0c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm6 0c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm-2 6c-3.3 0-6 3.3-6 4s2.7 4 6 4 6-3.5 6-4-2.7-4-6-4zm0 1.5V17c0 .6.4 1 1 1h1.5c0 1.6-1.5 2.8-3.2 2.4-.8-.2-1.5-.9-1.8-1.8-.3-1.6.9-3.1 2.5-3.1z" />
    </svg>
  </ShowButton>
);

export default StyledInput;
