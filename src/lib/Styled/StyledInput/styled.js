import styled from "styled-components";

export const Input = styled.input`
  outline: none;
  padding: 1rem 0.5rem;
  font-size: 1rem;
  border: none;
  background-image: none !important;
  color: black;
  border-radius: 1px;
  text-transform: uppercase;
  letter-spacing: 0.05em;
  font-weight: 500;
  position: relative;
  background-color: transparent;
  width: 100%;
  border-bottom: 2px solid ${props => props.theme.primary};
  transition: 0.3s;
  box-sizing: border-box;

  :focus {
    border-bottom: 2px solid ${props => props.theme.tertiary};
    transition: 0.3s;
    text-transform: none;
  }
`;

export const Container = styled.div`
  width: 100%;
  position: relative;
  margin: 1.2rem 0;
`;

export const ShowButton = styled.div`
  position: absolute;
  right: 0;
  top: 32%;
  width: 25px;
  cursor: pointer;
  transition: 0.3s;

  svg {
    transition: 0.3s;
    fill: ${props => (props.active ? `${props.theme.secondary}` : "#7a7a7a")};
  }
`;
