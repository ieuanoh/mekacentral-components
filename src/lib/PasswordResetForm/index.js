import React, { useState } from "react";
import PropTypes from "prop-types";
import MC from "mekacentral-js-sdk";
import Message from "../internals/components/Message";

/* Style imports */
import { TitleText, DescriptionText } from "./styled";
import StyledInput from "../Styled/StyledInput";
import StyledButton from "../Styled/StyledButton";
import StyledForm from "../Styled/StyledForm";

const PasswordResetForm = ({ ...props }) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [errorMsg, setError] = useState("");
  const [success, setSuccess] = useState(false);

  // Destructure props
  const { container, title, description, onSubmit, cssClass } = props;

  const submitReset = async () => {
    const res = await MC.send(
      () => MC.requestPasswordResetEmail(email),
      setLoading,
      setError
    );

    if (res) {
      setSuccess(true);
      // Invoke the onsubmit function if there is one specified
      onSubmit && onSubmit();
    }
  };

  const handleForm = e => {
    e.preventDefault();
    submitReset();
  };

  return (
    <StyledForm
      className={cssClass}
      container={container}
      onSubmit={e => handleForm(e)}
    >
      {title && <TitleText>{title}</TitleText>}
      {description && <DescriptionText>{description}</DescriptionText>}
      <StyledInput
        onChange={e => setEmail(e.target.value)}
        type="text"
        placeholder="Email"
        value={email}
        name="email"
      />

      {errorMsg && <Message error message={errorMsg} />}

      {success && (
        <Message
          success
          message={`Success! We have sent a password recovery email to ${email} `}
        />
      )}

      {!success && (
        <StyledButton
          loading
          type="submit"
          value={loading ? "Loading..." : "Request a Reset"}
        />
      )}
    </StyledForm>
  );
};

PasswordResetForm.propTypes = {
  /* Adds a styled container to the login form, true by default */
  container: PropTypes.bool,
  /* Adds a title to the form */
  title: PropTypes.string,
  /* Adds description text to the form beneath the text */
  description: PropTypes.string,
  /* Pass in an on submit function so an action can be specified when the reset is successful */
  onSubmit: PropTypes.func,
  /* Adds a cssClass to the component so styles can be overridden */
  cssClass: PropTypes.string
};

PasswordResetForm.defaultProps = {
  container: true,
  onSubmit: null,
  title: "Forgot your password?",
  cssClass: "",
  description:
    "Request a password reset link by using the form below with your MekaMon linked email."
};

export default PasswordResetForm;
