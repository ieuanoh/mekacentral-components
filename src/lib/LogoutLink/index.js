import React, { useContext } from "react";
import MC from "mekacentral-js-sdk";
import PropTypes from "prop-types";
import { Context } from "../MCProvider";

const LogoutLink = ({ ...props }) => {
    const AppState = useContext(Context);

    const handleLogout = async () => {
        await MC.logout();
        AppState.clearState("");
        window.location.pathname = props.redirect;
    };

    return (
        <p onClick={() => handleLogout()}>Logout</p>
    );
};

LogoutLink.propTypes = {
    linkText: PropTypes.string.isRequired,
    redirect: PropTypes.string.isRequired
};

LogoutLink.defaultProps = {
    linkText: "Logout",
    redirect: "/"
};

export default LogoutLink;
