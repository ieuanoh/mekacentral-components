import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    body, html {
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
    }

    *:focus {
      outline: none;
    }
`;

const ReachColours = {
  primary: "#000e37",
  secondary: "#00efee",
  tertiary: "#00a8f9",
  accent: "#46e846",
  darkest: "#04061D",
  dark: "#171A48",
};

const MekaColours = {
  primary: "#EB4444",
  secondary: "#FF671F",
  tertiary: "#F8D45D",
  darkest: "#04061D",
  dark: "#171A48",  
};

export const baseColours = {
  light: "#E4E4E4",
  lightest: "#EEEEEE",
  black: "#000000",
  "grey-darkest": "#3d4852",
  "grey-darker": "#606f7b",
  "grey-dark": "#8795a1",
  grey: "#b8c2cc"
};

export const screens = {
  sm: "576px",
  md: "768px",
  lg: "992px",
  xl: "1200px"
};

export const ReachTheme = {
  ...ReachColours,
  ...baseColours,
  ...screens
};
export const MekaTheme = {
  ...MekaColours,
  ...baseColours,
  ...screens
};
