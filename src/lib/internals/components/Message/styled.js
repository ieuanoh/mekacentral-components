import styled from "styled-components";
import { keyframes, css } from "styled-components";

const fadeInDown = keyframes`
from {
    transform: translateY(-5px);
    opacity: 0;
  }

  to {
    transform: translateY(0);
    opacity: 1;
  }
`;

const animation = () => css`
  ${fadeInDown} .8s;
`;

const colourScheme = {
  error: `
    background-color: #fce2de;
    color: #f44268;
  `,
  success: `
    background-color: #def2d6;
    color: #4e6246;
  `,
  warning: `
    background-color: #f8f3d6;
    color: #8b6b32;
  `
};

export const Container = styled.div`
  ${props => props.error && colourScheme.error}
  ${props => props.success && colourScheme.success}
  ${props => props.warning && colourScheme.warning}

  border-radius: 4px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0.8rem;
  margin-top: 0.3rem;
  margin-bottom: 0.3rem;
  animation: ${animation};

  p {
    margin: 0;
    font-size: 0.8rem;
  }
`;

export const Close = styled.img`
  width: 10px;
`;
