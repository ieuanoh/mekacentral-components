import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Container, Close } from "./styled";
import datauri from "../../../assets/datauri";

const Message = ({ ...props }) => {
  const { message, error, success, warning } = props;
  const [messageVisible, setVisibility] = useState(true);

  // If user has closed the message box,
  // we check if the incoming message has changed so we can refresh the state and show the new one

  useEffect(() => {
    setVisibility(true);
  }, [message]);

  return (
    <React.Fragment>
      {messageVisible && (
        <Container error={error} success={success} warning={warning}>
          <p>{message}</p>
          <Close
            onClick={() => setVisibility(false)}
            src={
              warning
                ? datauri.closeYellow
                : success
                ? datauri.closeGreen
                : datauri.close
            }
          />
        </Container>
      )}
    </React.Fragment>
  );
};

Message.propTypes = {
  message: PropTypes.string.isRequired,
  error: PropTypes.bool,
  success: PropTypes.bool,
  warning: PropTypes.bool
};

Message.defaultProps = {
  message: "An unknown error has occured",
  error: true,
  success: false,
  warning: false
};

export default Message;
