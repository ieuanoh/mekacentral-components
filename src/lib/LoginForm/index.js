import React, { useState, useRef, useContext } from "react";
import MC from "mekacentral-js-sdk";
import PropTypes from "prop-types";
import Message from "../internals/components/Message";
import { Context } from "../MCProvider";

/* Style imports */
import { Logo } from "./styled";
import StyledInput from "../Styled/StyledInput";
import StyledButton from "../Styled/StyledButton";
import StyledForm from "../Styled/StyledForm";

const Form = ({ ...props }) => {
  const [userName, setUsername] = useState("");
  const [loading, setLoading] = useState(false);
  const [errorMsg, setError] = useState("");
  const [loginSuccess, setSuccess] = useState(false);
  const passwordField = useRef(null);
  const AppState = useContext(Context);

  // Destructure props
  const { onSubmit, standardLogin, container, logo, cssClass } = props;

  const login = async standardLogin => {
    const res = await MC.send(
      () =>
        standardLogin
          ? MC.login(userName, passwordField.current.value)
          : MC.adminLogin(userName, passwordField.current.value),
      setLoading,
      setError
    );

    if (res) {
      setSuccess(true);
      // Update the AppState context with the successful login details
      AppState.updateUser(userName);
      // Fire the on submit function passed in as props
      onSubmit();
    }
  };

  const handleLogin = e => {
    e.preventDefault();
    // Call login depending on whether standard login is specified in props
    if (standardLogin) {
      login(true);
    } else {
      login();
    }
  };

  return (
    <StyledForm
      className={cssClass}
      loginSuccess={loginSuccess}
      container={container}
      onSubmit={e => handleLogin(e)}
    >
      {logo && <Logo src={logo} />}
      <StyledInput
        onChange={e => setUsername(e.target.value)}
        type="text"
        placeholder="Email"
        value={userName}
        name="username"
      />
      <StyledInput
        forwardedRef={passwordField}
        type="password"
        placeholder="Password"
        name="password"
      />
      {errorMsg && <Message error message={errorMsg} />}
      <StyledButton
        loading
        type="submit"
        value={loading ? "Loading..." : "Login"}
      />
    </StyledForm>
  );
};

Form.propTypes = {
  /* Adds a styled container to the login form, true by default */
  container: PropTypes.bool,
  /* Optional: specify a logo, requires a url string - not set by default */
  logo: PropTypes.string,
  /* Pass in an on submit function so an action can be specified when the login is successful */
  onSubmit: PropTypes.func.isRequired,
  /* Specify whether the form should be a standard, non admin login, by default this is set to false so that admin login is the default login method */
  standardLogin: PropTypes.bool
};

Form.defaultProps = {
  container: true,
  logo: "",
  standardLogin: false
};

export default Form;
