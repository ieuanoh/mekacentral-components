import styled from "styled-components";

export const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  max-width: 40rem;

  ${props =>
    props.container &&
    `
      padding: 2rem;
      background-color: white;
      box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.12), 0 2px 4px 0 rgba(0, 0, 0, 0.08);
    `}
`;

export const Logo = styled.img`
  text-align: center;
  align-self: center;
  max-width: 100%;
  width: 10rem;
  padding-bottom: 1rem;
`;
