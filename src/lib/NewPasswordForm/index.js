import React, { useState, useRef, useEffect } from "react";
import MC from "mekacentral-js-sdk";
import PropTypes from "prop-types";
import Message from "../internals/components/Message";

/* Style imports */
import StyledInput from "../Styled/StyledInput";
import StyledButton from "../Styled/StyledButton";
import StyledForm from "../Styled/StyledForm.js";

const Form = ({ ...props }) => {
  const [email, setEmail] = useState(false);
  const [token, setToken] = useState(false);
  const [loading, setLoading] = useState(false);
  const [errorMsg, setError] = useState("");
  const [loginSuccess, setSuccess] = useState(false);
  const passwordField = useRef(null);
  const confirmPasswordField = useRef(null);

  useEffect(() => {
    passwordField.current.focus();
    const urlParams = new URLSearchParams(window.location.search);
    setEmail(urlParams.get("email"));
    setToken(urlParams.get("token"));
  }, []);

  // Destructure props
  const { passwordPolicy, container, cssClass } = props;

  const passwordsMatch = () => {
    if (passwordField.current.value !== confirmPasswordField.current.value) {
      setError("Passwords do not match.");
      return false;
    }
    return true;
  };

  const validatePassword = () => {
    const passwordText = passwordField.current.value;

    for (let policy of passwordPolicy) {
      if (!passwordText.match(policy.rule)) {
        setError(policy.message);
        return false;
      }
    }
    setError("");
    return true;
  };

  const updatePassword = async () => {
    const res = await MC.send(
      () => MC.resetPassword(email, token, passwordField.current.value),
      setLoading,
      setError
    );

    if (res) {
      setSuccess(true);
    }
  };

  const handlePasswordReset = e => {
    e.preventDefault();
    const active = document.activeElement;

    if (active === passwordField.current) {
      if (validatePassword()) {
        // Move to second input
        confirmPasswordField.current.focus();
      }
    } else {
      if (validatePassword() && passwordsMatch()) {
        updatePassword();
      }
    }
  };

  return (
    <StyledForm
      className={cssClass}
      loginSuccess={loginSuccess}
      container={container}
      onSubmit={e => handlePasswordReset(e)}
    >
      <StyledInput
        forwardedRef={passwordField}
        onChange={e => validatePassword(e.target.value)}
        type="password"
        placeholder="New password..."
        name="password"
      />
      <StyledInput
        forwardedRef={confirmPasswordField}
        type="password"
        placeholder="Confirm new password..."
        name="confirmPassword"
      />
      {errorMsg && <Message error message={errorMsg} />}
      <StyledButton
        loading
        type="submit"
        value={loading ? "Loading..." : "Set New Password"}
      />
    </StyledForm>
  );
};

Form.propTypes = {
  passwordPolicy: PropTypes.array
};

// Password policy should probably be defined elsewhere but it's here for now
Form.defaultProps = {
  passwordPolicy: [
    {
      rule: /^.{8,}$/,
      message: "At least 8 characters."
    },
    {
      rule: /^.*[A-z]+.*$/,
      message: "At least one letter."
    },
    {
      rule: /^.*[^A-z\s]+.*$/,
      message: "At least one non-letter."
    }
  ]
};

export default Form;
