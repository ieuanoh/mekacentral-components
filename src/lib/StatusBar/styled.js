import styled from "styled-components";

export const Container = styled.nav`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0rem 0rem 0 1.5rem;
  z-index: 99;
  top: 0;
  left: 0;
  right: 0;
  box-shadow: 0 0px 0px 0 rgba(0, 0, 0, 0.08), 0 1px 1px 0 rgba(0, 0, 0, 0.08);
  background-color: ${props => (props.dark ? props.theme.primary : "#fff")};
  color: ${props => (props.dark ? "#fff" : props.theme.primary)};
`;

export const TitleContainer = styled.div`
  display: flex;
  align-items: center;
  padding: 1.5rem 0rem;

  h1 {
    margin: 0;
    font-size: 1rem;
  }
`;

export const StatusContainer = styled.div`
  display: flex;
`;

export const StatusItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-left: 1.5px solid #f5f5f5;
  padding: 1.5rem 2rem 1.5rem 1.5rem;

  h2 {
    margin: 0;
    font-size: 1rem;
    display: block;
    cursor: pointer;
  }
`;
export const UserIcon = styled.img`
  width: 1.2rem;
  padding-right: 0.4rem;
`;

export const StatusCircle = styled.div`
  height: 10px;
  width: 10px;
  margin-right: 7px;
  border-radius: 100%;
  background-color: ${props => props.theme.secondary};
`;
