import React, { useContext } from "react";
import MC from "mekacentral-js-sdk";
import PropTypes from "prop-types";
import {
  Container,
  StatusItem,
  UserIcon,
  TitleContainer,
  StatusContainer
} from "./styled";
import { Context } from "../MCProvider";
import datauri from "../assets/datauri";

const StatusBar = ({ ...props }) => {
  const AppState = useContext(Context);

  const handleLogout = async () => {
    await MC.logout();
    AppState.clearState("");
  };

  return (
    <Container dark={props.dark}>
      <TitleContainer>
        <h1>{props.appTitle}</h1>
      </TitleContainer>
      <div>
        {AppState.userName && (
          <StatusContainer>
            <StatusItem>
              <UserIcon src={datauri.userDefault} />
              <h2>{AppState.userName}</h2>
            </StatusItem>
            <StatusItem>
              <h2 onClick={() => handleLogout()}>Logout</h2>
            </StatusItem>
          </StatusContainer>
        )}
      </div>
    </Container>
  );
};

StatusBar.propTypes = {
  appTitle: PropTypes.string.isRequired
};

StatusBar.defaultProps = {
  appTitle: "Reach Admin Dashboard"
};

export default StatusBar;
