// This file just handles the exports of the components we want to include in the library

// Internal components
import Message from "./internals/components/Message";
import datauri from "./assets/datauri";

// Theme
import MCProvider from "./MCProvider";
import { MekaTheme, ReachTheme } from "./themes";

// Exported Components
import LoginForm from "./LoginForm";
import PasswordResetForm from "./PasswordResetForm";
import NewPasswordForm from "./NewPasswordForm";
import ResetPlayerAlias from "./ResetPlayerAlias";
import StatusBar from "./StatusBar";
import LogoutLink from "./LogoutLink";

// Exported helpers
import { Context } from "./MCProvider";

// Exports
export {
  Context,
  Message,
  datauri,
  MCProvider,
  StatusBar,
  LoginForm,
  LogoutLink,
  PasswordResetForm,
  NewPasswordForm,
  ResetPlayerAlias,
  MekaTheme,
  ReachTheme
};
