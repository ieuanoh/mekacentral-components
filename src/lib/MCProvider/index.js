import React, { useState, useEffect } from "react";
import MC from 'mekacentral-js-sdk';
import { ThemeProvider } from "styled-components";
import { GlobalStyle, baseColours, screens } from "../themes";
import PropTypes from "prop-types";
import { ThemeMap, BaseURL } from './defaults'

export const Context = React.createContext();

const MCProvider = ({ ...props }) => {
  
  const [userName, updateUser] = useState("");
  const [role, updateRole] = useState("");
  const [alias, updateAlias] = useState("");
  const [themeParam, setThemeParam] = useState("");


  useEffect(() => {
    // Check the query params for a theme parameter
    // If specified, overwrite the theme
    const urlParams = new URLSearchParams(window.location.search);
    const customTheme = urlParams.get('theme');
    const domain = urlParams.get('domain');
    setThemeParam(customTheme);
    // Custom environment domain overrides props, props overrides defaults.
    const baseUrl = domain || props.customEnvironment || BaseURL[props.MCEnvironment];
    MC.setBaseUrl(baseUrl);
  }, [])

  const appState = {
    userName: userName,
    role: role,
    alias: alias,
    updateUser: value => updateUser(value),
    updateRole: value => updateRole(value),
    updateAlias: value => updateAlias(value),
    /* Refresh state function used on logout in conjunction with the logout endpoont */
    clearState: () => {
      updateUser("");
      updateRole("");
      updateAlias("");
    }
  };

  // Merge custom theme with baseColours and breakpoints
  const customTheme = {
      ...props.customTheme, 
      ...baseColours, 
      ...screens
  }
  // Set theme - customTheme will trump everything, followed by the theme set in query params
  const theme = props.customTheme ? customTheme : ThemeMap[themeParam || props.theme]

  return (
    <Context.Provider value={appState}>
      <GlobalStyle />
      <ThemeProvider theme={theme}>{props.children}</ThemeProvider>
    </Context.Provider>
  );
};

MCProvider.propTypes = {
  /* Theme object which sets the color scheme for the entire component set, must be one of two preset themes - mekamon or reach */
  theme: PropTypes.oneOf(['mekamon', 'reach']),
  /* Custom theme object which will overwrite any preset themes, must be supplied as an object primary, secondary, tertiary, accent, etc hex values */
  customTheme: PropTypes.shape({
    primary: PropTypes.string.isRequired,
    secondary: PropTypes.string.isRequired,
    tertiary: PropTypes.string.isRequired,
    accent: PropTypes.string.isRequired,
    darkest: PropTypes.string.isRequired,
    dark: PropTypes.string.isRequired,
  }),
  /* The MC environment - determines what base URL endpoint is being hit, by default this is tes (https://test.mekacentral.mekamon.com) */
  MCEnvironment: PropTypes.oneOf(['dev', 'test', 'prod']),
  /* A base URL string indicating a custom environment - this will override any preset envs as the baseUrl if specified */
  customBaseUrl: PropTypes.string,
};

MCProvider.defaultProps = {
  theme: "mekamon",
  MCEnvironment: "test",
  customBaseUrl: '',
  customTheme: undefined,
};

export default MCProvider;
