import { MekaTheme, ReachTheme } from "../themes";

export const ThemeMap = {
    "mekamon": MekaTheme,
    "reach": ReachTheme,
  }

export const BaseURL = {
    "test" : 'https://test.mekacentral.mekamon.com',
    "dev" : 'https://dev.mekacentral.mekamon.com',
    "prod" : 'https://mekacentral.mekamon.com',
}