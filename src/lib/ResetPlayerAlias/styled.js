import styled from "styled-components";

export const TitleText = styled.h1`
  font-size: 1.8rem;
  margin: 0;
  color: ${props => props.theme.primary};
`;

export const DescriptionText = styled.p`
  color: black;
  opacity: 0.5;
`;
