import React, { useState } from "react";
import PropTypes from "prop-types";
import MC from "mekacentral-js-sdk";
import Message from "../internals/components/Message";

/* Style imports */
import { TitleText, DescriptionText } from "./styled";
import StyledInput from "../Styled/StyledInput";
import StyledButton from "../Styled/StyledButton";
import StyledForm from "../Styled/StyledForm";

const ResetPlayerAlias = ({ ...props }) => {
  const [email, setEmail] = useState("");
  const [loading, setLoading] = useState(false);
  const [errorMsg, setError] = useState("");
  const [success, setSuccess] = useState(false);
  const [alias, setAlias] = useState("");

  // Destructure props
  const { container, title, description, onSubmit, cssClass } = props;

  const submitReset = async () => {
    const res = await MC.send(
      async () => {
        const getAccount = await MC.getAccountByEmail(email);
        const key = Object.keys(getAccount.model)[0];
        const uuid = getAccount.model[key].playerUuid;
        const reset = await MC.resetPlayerAlias(uuid);
        return reset;
      },
      setLoading,
      setError
    );

    if (res) {
      setSuccess(true);
      setAlias(res.properties.alias);
      // Invoke the onsubmit function if there is one specified
      onSubmit && onSubmit();
    }
  };

  const handleForm = e => {
    e.preventDefault();
    submitReset();
  };

  return (
    <StyledForm
      className={cssClass}
      container={container}
      onSubmit={e => handleForm(e)}
    >
      {title && <TitleText>{title}</TitleText>}
      {description && <DescriptionText>{description}</DescriptionText>}
      <StyledInput
        onChange={e => setEmail(e.target.value)}
        type="text"
        placeholder="Email"
        value={email}
        name="email"
      />

      {errorMsg && <Message error message={errorMsg} />}

      {success && (
        <Message
          success
          message={`Success! New player alias for ${email} set as ${alias}`}
        />
      )}

      {!success && (
        <StyledButton
          loading
          type="submit"
          value={loading ? "Loading..." : "Reset Alias"}
        />
      )}
    </StyledForm>
  );
};

ResetPlayerAlias.propTypes = {
  /* Adds a styled container to the login form, true by default */
  container: PropTypes.bool,
  /* Adds a title to the form */
  title: PropTypes.string,
  /* Adds description text to the form beneath the text */
  description: PropTypes.string,
  /* Pass in an on submit function so an action can be specified when the reset is successful */
  onSubmit: PropTypes.func,
  /* Adds a cssClass to the component so styles can be overridden */
  cssClass: PropTypes.string
};

ResetPlayerAlias.defaultProps = {
  container: true,
  onSubmit: null,
  title: "Reset Player Alias",
  cssClass: "",
  description: ""
};

export default ResetPlayerAlias;
