// Test file to show example usage of an MC component
// In production, the import would be from a hosted NPM module and not the lib folder

import React from "react";
import { render } from "react-dom";
import { LoginForm } from "./lib";
import { MCProvider } from "./lib";
import { Container, InnerContainer } from "./styled";
import "./style.css";

const testSuccess = () => {
  // Typically, perform a router push or something here to redirect users
  alert("You are logged into MC");
};

const App = () => (
  <MCProvider theme="mekamon">
    <React.Fragment>
      <Container>
        <InnerContainer>
          <LoginForm onSubmit={testSuccess} standardLogin />
        </InnerContainer>
      </Container>
    </React.Fragment>
  </MCProvider>
);

render(<App />, document.getElementById("root"));
