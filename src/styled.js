import styled from "styled-components";

export const Container = styled.section`
  width: 100%;
  height: 100vh;
  position: fixed;
  display: flex;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.theme.primary};
  background-repeat: no-repeat;
  background-position: bottom right;
`;

export const InnerContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 35rem;
  margin-top: -4rem;
  padding: 3rem;
  transition: 0.5s;
`;
